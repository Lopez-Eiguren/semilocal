#!/bin/bash
# SGE variables

# Shell used to run the task on SGE
#$ -S /bin/bash

# Name of the job
#$ -N  sl-b01

# Specifies whether or not the standard error stream of the job 
# is merged into the standard output stream
#$ -j y

# SGE Environment Variables:
# HOME  Home directory on execution machine
# USER  User ID of job owner
# JOB_ID  Current job ID
# JOB_NAME  Current job name; see the -N option
# HOSTNAME  Name of the execution host
# TASK_ID  Array job task index number

# Path where will be stored output (-o option)
# and error files (opcion -e)
#$ -o $JOB_NAME.$JOB_ID
##$ -o $JOB_NAME.$JOB_ID.$TASK_ID

# With this option error and output files are stored in the 
# directory from where the job is submitted using qsub
#$ -cwd

# E-mail address where the status of the job will be send
#$ -M asier.lopez@ehu.es

# When an email is sent to inform the user about the status of
# a job: -m e (end of a job), -m b (before of a job)
#$ -m be

# If the job is parallel, we indicate what kind of environment
# we use mpich for MPICH jobs or openmp for OPENMP jobs
# and the number of processes to use
#$ -pe openmpi 144

# Request a maximum of 12 hours of execution time
# This parameter is commented because this job lasts weeks
##$ -l h_rt=50:00:00

# Request 1GB of RAM memory for the execution
#$ -l vf=2000M

# Multiple jobs to execute from-to:step
# In this example only one job
##$ -t 1-1:1

# Which queues to use?
# There are multiple queues in the system:
# gen.q: queue for general users with short jobs. 
#$ -hard -q all.q

# With the -V option all environment variables are exported
# to SGE but using -v only the specified variable is exported
#$ -v OMP_NUM_THREADS=1
#$ -v P4_GLOBMEMSIZE=32000000
#$ -v P4_RSHCOMMAND=/usr/bin/ssh
#$ -V
echo Running on host $HOSTNAME
echo Directory is `pwd`
ORIG_DIR=`pwd`

# create temporary directory to copy input and support files later
#TEMP_DIR=/var/tmp/`whoami`.$$
#echo TEMP_DIR es $TEMP_DIR
#mkdir $TEMP_DIR
#echo $TMPDIR

# Variables needed to run the job 
MPIRUN=/opt/openmpi-intel/1.2.4/bin/mpirun

# Put the full path because in parallel environment openmp
# the path is not saved


# copy input and support files to a temporary directory on compute node
#cp $INPUT_FILE $AUXILIAR_FILES $MACHINEFILE $TEMP_DIR
#cp * $TEMP_DIR
#cd $TEMP_DIR
echo This job has allocated $NSLOTS processors
echo Init time is `date`


#run the job

cd /home/alopez/Simulazioa-Semilocal

$MPIRUN -np 144 ./LSL1 -s setting.sf -l 12 -m 12  > test.out


echo End time is `date`

# copy files back and clean up
#cp * $ORIG_DIR
#rm -rf $TEMP_DIR

