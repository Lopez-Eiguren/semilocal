#ifndef LOGSPACEDTIMESTEP_HPP
#define LOGSPACEDTIMESTEP_HPP

#include <cmath>
#include "xmath.hpp"

bool logSpacedTimeStep(int step, int number, int startStep, int endStep)
{
  double dlog;     //Increment in log(step/startStep) required
  int acceptStep;
  int lastAcceptStep = startStep;

  //If step is the startStep, then definately accept
  if(step==startStep) { return 1; }

  //Calculate preliminary dlog value
  dlog = log10( double(endStep)/double(startStep) ) / double(number-1);

  for(int i=2; i<=number; i++)
    {
      //Calculate nearest integer to log required 
      acceptStep = roundNearest( lastAcceptStep * pow(double(10), dlog) );

      //If step unchanged, then increase by one
      if(acceptStep == lastAcceptStep) { acceptStep++; }

      //Store last step 
      lastAcceptStep = acceptStep;

      //Redistribute logs over remaining steps
      dlog = log10( double(endStep)/double(acceptStep) ) / double(number-i);

      if(step == acceptStep) 
	{ 
	  return 1; 
	}
    }

  return 0;
}

#endif
