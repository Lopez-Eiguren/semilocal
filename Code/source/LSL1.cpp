#include <cstdlib>
#include <iostream>
#include <cmath>
#include <ctime>
#include <climits>

using namespace std;

#include "LATfield2d.hpp"
using namespace LATfield2d;




#include "LSLsimulation.hpp"
#include "LSLsim.cpp"
#include "int2string.hpp"
#include "hourMinSec.hpp"
#include "SettingsFile.hpp"
#include "xmath.hpp"
#include "RandGen.hpp"
#include "logSpacedTimeStep.hpp"



int main(int argc, char **argv)
{
int nCore,mCore;
double t;
int initialConditionsType;
 Real dt;
 Real damping;
string fileSettings;
string runID="";
bool verifArg[2];
verifArg[0]=false;
verifArg[1]=false;
double tt;

for (int i=1 ; i < argc ; i++ )
    {
	if ( argv[i][0] != '-' )continue;
	switch(argv[i][1]) 
        {
	    case 'i':
		runID = argv[++i];
		verifArg[1]=true;
		break;
	    case 's':
		fileSettings = argv[++i];
		verifArg[0]=true;
		break;
	    case 'l': 
		nCore = atoi(argv[++i]);
		break;
	    case 'm':
		mCore =  atoi(argv[++i]);
		break;
	}
    }

  if(!verifArg[0])
    {
        COUT<<"Usage:"<<argv[0]<<" -s settingsFileName [-i runIDstring] -l nCore -m mCore"<<endl;
        COUT<<"Exiting..."<<endl;
        parallel.abortRequest();
    }
	
    parallel.initialize(nCore,mCore);

   
    LSLsimulation sim;

    SettingsFile setfile;


    setfile.open(fileSettings, SettingsFile::autoCreate, argc-3, argv+3);

    setfile.read("path", sim.path);

    setfile.read("id",sim.id);

    setfile.read("seeda",sim.seeda);

    setfile.read("dim", sim.dim);

    setfile.read("N", sim.N);

    setfile.read("dx", sim.dx);

    setfile.read("dt", sim.dt);

    setfile.read("factor", sim.factor);

    setfile.read("smooth",sim.smooth);

    setfile.read("Beta",sim.Beta);

    setfile.read("fluxmax",sim.fluxmax);

    setfile.read("Threshold",sim.Threshold);

    setfile.read("tinit",sim.tinit);

    setfile.read("tend",sim.tend);

    setfile.read("DAMP",sim.DAMP);

    setfile.read("hDAMP",sim.hDAMP);

    setfile.read("tendDAMP",sim.tendDAMP);

    setfile.read("tinitout", sim.tinitout);

    setfile.read("tendout",sim.tendout);

    setfile.read("tstepout",sim.tstepout);

    
    setfile.close();

    if(parallel.rank()==0)
      {
	string sortu="mkdir "+sim.path;

	const char* simpath = sortu.c_str();

	system(simpath);
      }


    sim.initialize();
    
	sim.emAlloc();
  
    COUT<<"Number of processes: "<<parallel.size()<<endl;
    COUT<<endl;
    COUT<<"dx="<<sim.dx<<endl;
    COUT<<"dt="<<sim.dt<<endl;

    sim.DL1=1.0/sim.dx;
    sim.DL2=sim.DL1*sim.DL1;
    damping=sim.DAMP;

	
   sim.initialConditions(1,sim.seeda);

  
	
  
   
   


   for(t=sim.tinit; t<(sim.tend+sim.dt); t=t+sim.dt){
  	

	if(t<sim.tendDAMP){sim.DAMP=sim.hDAMP;}
	else{sim.DAMP=damping/t;}
	

	//sim.GaussTest(t);
   	sim.energia(t);
  	//sim.abiadurak(t);
   	sim.evolve();
   

 

	
       
	
   if(t>=sim.tinitout and t<=sim.tendout and int((t-sim.tinitout)*100)%int(sim.tstepout*100)==0 and int(t-sim.tinitout)%int(sim.tstepout)==0){
    sim.output(t);}
     //sim.Monopoles(t);}
  
   }

 
}


