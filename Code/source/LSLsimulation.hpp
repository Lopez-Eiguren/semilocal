/* Soka semilokalak simulatzeko kodigoa latfield-en idatzita */

#ifndef LSLSIMULATION_HPP
#define LSLSIMULATION_HPP


//#include "LATfield2d.hpp"
//using namespace LATfield2d;


/* Behar diren paketeak hemen sartu behar dira

#include "_____.hpp"

eran idatzita. */


#include "constants.hpp"
#include "RandGen.hpp"
#include "Simulation.hpp"
#include "xmath.hpp"
#include "int2string.hpp"
#include "complex.h"


class LSLsimulation : public Simulation
{
public:
/* Hemen simulazioak erabiliko dituen aldagai eta funztio guztiak definitu behar dira. */ 


//Eremuak
Field<Imag> phi1;
Field<Imag> phi2;
Field<Imag> pi1;
Field<Imag> pi2;
Field<Real> theta;
Field<Real> epsilon;
Field<Real> ki1;
Field<Real> roki;
Field<Real> ki;
Field<Imag> p1;
Field<Imag> p2;


//Simulazioaren berezko parametroak


Real DL1;
Real DL2;
Real DAMP;
  Real hDAMP;
  Real tendDAMP;
  Real tend;
  Real tinit;
  Real tinitout;
  Real tendout;
  Real tstepout;
int Threshold;
int smooth;
Real factor;
Real Beta;
Real fluxmax;
int  initialConditionsType;
  string path;
  string id;
  int seeda;


//CONSTRUCTOR
 LSLsimulation();

//INITIALIZATION
void initialize();

//CORE FUNCTIONS
void evolve();
void initialConditions(int type, int seed);
void initialConditionsType1(int seed);
void output(double t);
void Monopoles(double t);
double computearea(double a[3], double b[3], double c[3]);
double computearea1(double a[3], double b[3], double c[3]);
double dot(const double a[3], const double b[3]);
void abiadurak(double t);
void Winding(double t);
int windingPlacket1(Site& x, int i, int j);
int windingPlacket2(Site& x, int i, int j);
void emTensorWinding(Site& x,double t, Real* T);
//HIBERNATION FUNCTIONS
void hibernateSave();
void hibernateLoad();

//MISCENALEOUS FUNCTIONS
void statsFileLine();
void first();
void next();
void GaussTest(double t);
void energia(double t);
};


#endif


























