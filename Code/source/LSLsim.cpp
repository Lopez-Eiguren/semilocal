
#include "LSLsimulation.hpp"

//==================================================
//CONSTRUCTOR=======================================
//==================================================

LSLsimulation::LSLsimulation(){



initialConditionsType=1;
}

//==================================================
//INITIALIZATION====================================
//==================================================

void LSLsimulation::initialize()
{
  lattice.initialize(dim, N, 1);
  phi1.initialize(lattice);
  phi2.initialize(lattice);
  pi1.initialize(lattice);
  pi2.initialize(lattice);
  theta.initialize(lattice,dim);
  epsilon.initialize(lattice,dim);
  p1.initialize(lattice);
  p2.initialize(lattice);
  phi1.alloc();
  phi2.alloc();
  pi1.alloc();
  pi2.alloc();
  theta.alloc();
  epsilon.alloc(); 
  p1.alloc();
  p2.alloc();
  ki1.initialize(lattice,3);
  roki.initialize(lattice,3);
  ki.initialize(lattice,3);
  ki1.alloc();
  roki.alloc();
  ki.alloc();

}


//==================================================
//CORE FUNCTIONS====================================
//==================================================

void LSLsimulation::evolve()
{
Site x(lattice);

int i;
int j;

//Eboluzioan behar diren parametroak
double  dp=(1.0-0.5*DAMP);
double  dm=1.0/(1.0+0.5*DAMP);
Imag  deltaPi1;
Imag  deltaPi2;
double* deltaEpsilon = new double[dim];
double  a;
double  b;
double th;




//Evolve primary fields
 for( x.first(); x.test(); x.next() )
   {
     phi1(x) += dt*pi1(x);
     phi2(x) += dt*pi2(x);

     for(i=0; i<dim; i++){ theta(x,i) += dt*epsilon(x,i);}
   }


 phi1.updateHalo();

 phi2.updateHalo();

 theta.updateHalo();

//Evolve time derivative fields

 for( x.first(); x.test(); x.next() )
   {


     pi1(x)= dm*(dp*pi1(x)+dt*phi1(x)*(-Beta*(phi1(x).norm()+phi2(x).norm()-1.0)-6.0*DL2));
     pi2(x)= dm*(dp*pi2(x)+dt*phi2(x)*(-Beta*(phi2(x).norm()+phi1(x).norm()-1.0)-6.0*DL2));

     for(i=0; i<dim; i++)
       {
	th=theta(x,i);

	 pi1(x)+=dm*dt*DL2*(expi(dx*theta(x,i))*phi1(x+i)+expi(-dx*theta(x-i,i))*phi1(x-i));

	 pi2(x)+=dm*dt*DL2*(expi(dx*theta(x,i))*phi2(x+i)+expi(-dx*theta(x-i,i))*phi2(x-i));

	 epsilon(x,i) = dm*(dp*epsilon(x,i)-dt*DL1*(phi1(x).conj()*expi(dx*th)*phi1(x+i)-phi1(x+i).conj()*expi(-dx*th)*phi1(x)+phi2(x).conj()*expi(dx*th)*phi2(x+i)-phi2(x+i).conj()*expi(-dx*th)*phi2(x)).imag());
				
  
	 for(j=(i==0); j<dim; j++, j+=(j==i))
	   {
	     epsilon(x,i) += dm*dt*DL1*DL2*(sin(dx*(theta(x,j)+theta(x+j,i)-theta(x+i,j)-theta(x,i)))-sin(dx*(theta(x-j,j)+theta(x,i)-theta(x+i-j,j)-theta(x-j,i))));
   
	   }
  
       }

   }

 delete[] deltaEpsilon;


 epsilon.updateHalo();
 pi1.updateHalo();
 pi2.updateHalo();

}



//==================================================
//INITIAL CONDITIONS================================
//==================================================

void LSLsimulation::initialConditions(int type,int seed)
{
  if(type==1)
    {
      //|pi1|=kte |pi2|=kte arg(pi1)=arg(pi2)=uniform random, phi1=0, phi2=0, theta=0, epsilon=0
      initialConditionsType1(seed);
       
     }

//Nahi izanez gero hasierako egoera gehiago sartu ditzazket


}


void LSLsimulation::initialConditionsType1( int seed)
{
  Site x(lattice);
  double phase1;
  double phase2;
  double a;
  double b;
  int  i,ze;
  double nor[4];
  double norm,norma,n[4],norma1,norm1;

  //Initialize random number generator
  randGen.initialize(seed,2,32);

	
    

  //Generate fields site by site
  for( x.first(); x.test(); x.next() )
    {
	do{
	for(ze=0,norma=0.0;ze<4;ze++)
	{
	 n[ze]=randGen.generateN();
	 norma+=n[ze]*n[ze];
	}	
	} while((norm=sqrt(norm)) > 1.0);


	for(ze=0;ze<4;ze++){n[ze]/=norma;}

       pi1(x).real() = n[0];
       pi1(x).imag() = n[1];
       pi2(x).real() = n[2];
       pi2(x).imag() = n[3];
   
       phi1(x)=Imag(0,0);
       phi2(x)=Imag(0,0);
       for(int i=0; i<dim; i++)
	 {
	   theta(x,i) = Real(0);
	   epsilon(x,i) = Real(0);
	 }


    }
	
  phi1.updateHalo();
  phi2.updateHalo();

	double ph1n,ph2n;

   for(int sm=0; sm<smooth; sm++)
    {

      for( x.first(); x.test(); x.next() )
	{
	
	p1(x)=6.0*pi1(x)+pi1(x+0)+pi1(x-0)+pi1(x+1)+pi1(x-1)+pi1(x+2)+pi1(x-2);

	p2(x)=6.0*pi2(x)+pi2(x+0)+pi2(x-0)+pi2(x+1)+pi2(x-1)+pi2(x+2)+pi2(x-2);
  
	}

      for( x.first(); x.test(); x.next() )
        {
	  norm=p1(x).norm()+p2(x).norm();

	  norm=sqrt(norm);

	  if(norm==0){norm=1.0;}

	  pi1(x)=p1(x)/norm*factor;
	  pi2(x)=p2(x)/norm*factor;

        }

	pi1.updateHalo();
  	pi2.updateHalo();
    }
 
  phi1.updateHalo();
  phi2.updateHalo();
  theta.updateHalo();
  pi1.updateHalo();
  pi2.updateHalo();
  epsilon.updateHalo();

}

//==================================================
//OUTPUT============================================
//==================================================

void LSLsimulation::output(double t)
{
    Site   x(lattice);
    double   fluxx;
    int    fl,fl1;
    FILE   *fout,*check,*filestr;
    int    i;
    int    j;
    int    rank;
    double    mod;
    double angle[24],angle2[24];
    double ta1,ta2,ta3,ta4,ta5,ta6,ta7,ta8,ta9,ta10,ta11,ta12;
    double ta21,ta22,ta23,ta24,ta25,ta26;
    double modu,modu1;
    double nor[2];
    double norm,normki,norm1;
    double e22,b2;
    double la,Fij, f0i,po,pipi,diphi,diphi1;
    double lae2,lab2,lapipi,ladiphi,tola,tolae2,tolab2,tolapipi,toladiphi;
    double e2l,b2l,pipil,diphil,v,v1;
    double veb,vpd;
    Real divEpsilon;
    double  sumSqPiConjPhiImag = 0.0;
    double  sumSqDivEpsilon = 0.0;
    Real    qGauss;
    bool    lowEdge;
    double sumF0i=Real(0);
    double sumFij=Real(0);
    double sumPi=Real(0);
    double sumDiPhi=Real(0);
    double sumV=Real(0);
    double corner[8][3];
    double ph1r,ph1i,ph2r,ph2i;


    
    ////ARTXIBO GUZTIAK ZABALDU///////////////////
    //////////////////////////////////////////////

    fstream fileEM,fileSO1,fileSO2,fileMO,fileMO1,fileAB, fileGA,fileEN;
    string  filenameEM,filenameSO1, filenameSO2,filenameMO,filenameMO1,filenameAB, filenameGA,filenameEN;
    char   nameoutEM[64],nameoutSO1[64], nameoutSO2[64],nameoutMO[64],nameoutMO1[64],nameoutAB[64];
    int izt;	
	
    izt=int(t);

    sprintf(nameoutEM,"%d-emaitza%d_",izt,parallel.rank());
    filenameEM = path+nameoutEM+id+".dat" ;
    fileEM.open(filenameEM.c_str(), fstream::out | fstream::app);


    sprintf(nameoutSO1,"%d-wind1%d_",izt,parallel.rank());
    filenameSO1=path+nameoutSO1+id+".dat";
    fileSO1.open(filenameSO1.c_str(), fstream::out | fstream::app);
    
    sprintf(nameoutSO2,"%d-wind2%d_",izt,parallel.rank());
    filenameSO2=path+nameoutSO2+id+".dat";
    fileSO2.open(filenameSO2.c_str(), fstream::out | fstream::app);

    sprintf(nameoutMO,"%d-monopolo%d_",izt,parallel.rank());
    filenameMO =path+nameoutMO+id+".dat" ;
    fileMO.open(filenameMO.c_str(), fstream::out | fstream::app);

    //sprintf(nameoutMO1,"%d-2monopolo%d_",t,parallel.rank());
    //filenameMO1 =path+nameoutMO1+id+".dat" ;
    //fileMO1.open(filenameMO1.c_str(), fstream::out | fstream::app);
    
    sprintf(nameoutAB,"abiadurak_");
    filenameAB = path+nameoutAB+id+".dat" ;
    fileAB.open(filenameAB.c_str(), fstream::out | fstream::app);

    filenameGA=path+"gauss_"+id+".dat";
    fileGA.open(filenameGA.c_str(), fstream::out | fstream::app);
    

    filenameEN = path+"energia_"+id+".dat" ;
    fileEN.open(filenameEN.c_str(), fstream::out | fstream::app);

  
//////////ABIADUREN INIZIALIZAZIOA//////////////////
    ///////////////////////////////////////////////
    
    tola=0.0;
    
    tolae2=0.0;
    
    tolab2=0.0;
    
    tolapipi=0.0;
    
    toladiphi=0.0;
    
    
    
 /////soka puntuz puntu lortzeko smooth-a ETA MONOPOLOETARAKO EREMUAK//////////////////
    ////////////////////////////////////////////////////////



 for( x.first(); x.test(); x.next() )
    {
        ki1(x,0)=(phi2(x).conj()*phi1(x)+phi1(x).conj()*phi2(x)).real();
        ki1(x,1)=(phi2(x).conj()*phi1(x)-phi1(x).conj()*phi2(x)).imag();
        ki1(x,2)=phi1(x).norm()-phi2(x).norm();

}

    	ki1.updateHalo();
///ROTAZIONALAREN ROTAZIONALA/////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////

    
    
    for(x.first(); x.test(); x.next() )
    {
        roki(x,0)=ki1(x,2)-ki1(x-1,2)-ki1(x,1)+ki1(x-2,1);
        roki(x,1)=ki1(x,0)-ki1(x-2,0)-ki1(x,2)+ki1(x-0,2);
        roki(x,2)=ki1(x,1)-ki1(x-0,1)-ki1(x,0)+ki1(x-1,0);
    
    }
    
    roki.updateHalo();
    
    for(x.first(); x.test(); x.next() )
    {
        ki(x,0)=roki(x,2)-roki(x-1,2)-roki(x,1)+roki(x-2,1);
        ki(x,1)=roki(x,0)-roki(x-2,0)-roki(x,2)+roki(x-0,2);
        ki(x,2)=roki(x,1)-roki(x-0,1)-roki(x,0)+roki(x-1,0);

    }

    ki.updateHalo();

    
    
    
////////////////KALKULUAK////////////////////////////////////////
////////////////////////////////////////////////////////////////
    
   

 for(x.first(); x.test(); x.next() )
{
    
    
    
    
    /////EREMU MAGNETIKOA//////////////////////
    //////////////////////////////////////////
    
      fluxx=3.0*DL2*DL2;

      for(i=0; i<dim; i++)
      {
        for(j=(i==0);j<dim; j++, j+=(j==i))
	    {
	      fluxx -=0.5*DL2*DL2*cos(dx*(theta(x,j)+theta(x+j,i)-theta(x+i,j)-theta(x,i)));
	    }
      }

      fl1=(int)(256.0*sqrt(fluxx)/fluxmax);
      if(fl1>256){fl=256;}
      else{fl=fl1;}

      if(fl>Threshold)
      {
		
          fileEM<<x.coord(0)<<" "<<x.coord(1)<<" "<<x.coord(2)<<" "<<fl<<"\n";


    
        double are=0.0;


        for(int g=0;g<3;g++)
        {
            corner[0][g]=ki1(x,g)-ki(x,g);
            corner[1][g]=ki1(x+1,g)-ki(x+1,g);
            corner[2][g]=ki1(x+0,g)-ki(x+0,g);
            corner[3][g]=ki1(x+0+1,g)-ki(x+0+1,g);
            corner[4][g]=ki1(x+2,g)-ki(x+2,g);
            corner[5][g]=ki1(x+1+2,g)-ki(x+1+2,g);
            corner[6][g]=ki1(x+0+2,g)-ki(x+0+2,g);
            corner[7][g]=ki1(x+0+1+2,g)-ki(x+0+1+2,g);
        }
        
        
        are+=computearea(corner[0],corner[1],corner[2]);/*1*/
        are+=computearea(corner[1],corner[3],corner[2]);/*2*/
        are+=computearea(corner[1],corner[5],corner[3]);/*3*/
        are+=computearea(corner[5],corner[7],corner[3]);/*4*/
        are+=computearea(corner[5],corner[4],corner[7]);/*5*/
        are+=computearea(corner[4],corner[6],corner[7]);/*6*/
        are+=computearea(corner[4],corner[0],corner[6]);/*7*/
        are+=computearea(corner[0],corner[2],corner[6]);/*8*/
        are+=computearea(corner[2],corner[3],corner[6]);/*9*/
        are+=computearea(corner[3],corner[7],corner[6]);/*10*/
        are+=computearea(corner[4],corner[5],corner[0]);/*11*/
        are+=computearea(corner[5],corner[1],corner[0]);/*12*/
	
        
        are/=(4*3.1418);


        if(abs(are)>0.9)fileMO<<x.coord(0)<<" "<<x.coord(1)<<" "<<x.coord(2)<<" "<<are<<"\n";
      //  if(abs(are1)>0.9)fileMO1<<x.coord(0)<<" "<<x.coord(1)<<" "<<x.coord(2)<<" "<<are1<<" "<<2<<"\n";
    }
   ///////WINDINGS//////////////////////////////////////
   ////////////////////////////////////////////////////
	int winding1=0;
	int winding2=0;
	int nw,iw,jw;
	Real T1[10];
	Real T2[10];
	for(nw=0, iw=0, jw=1; nw<3; nw++,iw=jw, jw=(iw+1)%3)
	{
		winding1+= abs(windingPlacket1(x,iw,jw));	
		winding2+= abs(windingPlacket2(x,iw,jw));
	}
	
	if(winding1>0.9)
	{
		emTensorWinding(x,t,T1);

		fileSO1<<x.coord(0)<<" "<<x.coord(1)<<" "<<x.coord(2)/*<<" "<<T1[0]<<" "<<T1[1]<<" "<<T1[2]<<" "<<T1[3]<<" "<<T1[4]<<" "<<T1[5]<<" "<<T1[6]<<" "<<T1[7]*/<<" "<<T1[8]<<" "<<T1[9]<<"\n";
	}
	if(winding2>0.9)
	{
		emTensorWinding(x,t,T2);

		fileSO2<<x.coord(0)<<" "<<x.coord(1)<<" "<<x.coord(2)/*<<" "<<T2[0]<<" "<<T2[1]<<" "<<T2[2]<<" "<<T2[3]<<" "<<T2[4]<<" "<<T2[5]<<" "<<T2[6]<<" "<<T2[7]*/<<" "<<T2[8]<<" "<<T2[9]<<"\n";
	}

    /////ABIADURAK///////////////////////////////////////
    ////////////////////////////////////////////////////
    
    b2=0.0;
    f0i=0.0;
    Fij=0.0;
    e22=0.0;
    diphi=0.0;
    pipi=0.0;
    po=0.0;
    
    for(i=0; i<dim; i++)
    {
        
        f0i+=epsilon(x,i)*epsilon(x,i);
        diphi+=((expi(dx*theta(x,i))*phi1(x+i)-phi1(x))/dx).norm()+((expi(dx*theta(x,i))*phi2(x+i)-phi2(x))/dx).norm();
        for(int j=(i==0); j<dim;j++,j+=(i==j))
        {
            Fij+=(1.0-cos(dx*(theta(x,j)+theta(x+j,i)-theta(x+i,j)-theta(x,i))));
        }
    }
    
    
    po=Beta*(phi1(x).norm()+phi2(x).norm()-1.0)*(phi1(x).norm()+phi2(x).norm()-1.0);
    
    pipi=pi1(x).norm()+pi2(x).norm();
    
    Fij/=dx*dx*dx*dx;
    
    la=Fij;
    
    lae2=f0i*la;
    
    lab2=Fij*la;
    
    lapipi=pipi*la;
    
    ladiphi=diphi*la;
    
    tola+=la;
    
    tolae2+=lae2;
    
    tolab2+=lab2;
    
    tolapipi+=lapipi;
    
    toladiphi+=ladiphi;

    
    
    ////GAUSS LAW/////////////////////////
    /////////////////////////////////////
    
    lowEdge=0;
    for(int i=0; i<dim; i++){if(x.coordLocal(i)==0){lowEdge = 1;}}
    if(!lowEdge)
        {
            sumSqPiConjPhiImag += pow((pi1(x).conj()*phi1(x)+pi2(x).conj()*phi2(x)).imag(),Real(2));
            divEpsilon=Real(0);
            for(int i=0; i<dim; i++) {divEpsilon += epsilon(x,i) - epsilon(x-i,i);}
            sumSqDivEpsilon += divEpsilon * divEpsilon;
        }

    
    //////////////ENERGIA///////////////////
    ///////////////////////////////////////
    
    sumPi += pi1(x).norm()+pi2(x).norm();
    sumV +=(phi1(x).norm()+phi2(x).norm()-1.0)*(phi1(x).norm()+phi2(x).norm()-1.0);
    
    for(int i=0; i<dim; i++)
    {
        sumF0i +=epsilon(x,i)*epsilon(x,i);
        sumDiPhi += ((expi(dx*theta(x,i))*phi1(x+i)-phi1(x))/dx).norm()+((expi(dx*theta(x,i))*phi2(x+i)-phi2(x))/dx).norm();
        for(int j=(i==0); j<dim;j++,j+=(i==j))
        {
            sumFij += 1.0-cos(dx*(theta(x,j)+theta(x+j,i)-theta(x+i,j)-theta(x,i)));
        }
    }
    
    
}
    
    ///ABIADUREN AZKEN KALKULUAK///////////////
    /////////////////////////////////////////
    
    parallel.sum(tola);
    parallel.sum(tolae2);
    parallel.sum(tolab2);
    parallel.sum(tolapipi);
    parallel.sum(toladiphi);
    
    
    e2l=tolae2/tola;
    b2l=tolab2/tola;
    pipil=tolapipi/tola;
    diphil=toladiphi/tola;
    
    v=e2l/b2l;
    veb=v;
    v1=pipil/diphil;
    vpd=2*v1/(1+v1);
    
    if(parallel.isRoot()){fileAB<<t<<" "<<veb<<" "<<vpd<<"\n";}
    
    ///////////GAUSS LAW AZKEN KALKULUAK///////////////
    ///////////////////////////////////////////////////
    
    sumSqDivEpsilon /= dx*dx;
    
    parallel.sum(sumSqPiConjPhiImag);
    parallel.sum(sumSqDivEpsilon);
    
    
    if(parallel.isRoot()){fileGA<<t<<" "<<2.0*sqrt(sumSqPiConjPhiImag/lattice.sites())<<" "<<sqrt(sumSqDivEpsilon/lattice.sites())<<endl;}
    
    ////////////ENERGIAREN AZKEN KALKULUAK///////////
    ////////////////////////////////////////////////
    
    sumF0i /=Real(2);
    sumFij /= Real(2)*dx*dx*dx*dx;
    sumV *=0.5*Beta;
    
    parallel.sum(sumF0i);
    parallel.sum(sumFij);
    parallel.sum(sumPi);
    parallel.sum(sumDiPhi);
    parallel.sum(sumV);
    
    
    
    if(parallel.isRoot()){fileEN<<t<<" "<<sumF0i/lattice.sites()<<" "<<sumFij/lattice.sites()<<" "<<sumPi/lattice.sites()<<" "<<sumDiPhi/lattice.sites()<<" "<<sumV/lattice.sites()<<" "<<sumF0i/lattice.sites()+sumFij/lattice.sites()+sumPi/lattice.sites()+sumDiPhi/lattice.sites()+sumV/lattice.sites()<<"\n";}
    
    
    
   
    fileEM.close();
    fileSO1.close();
    fileSO2.close();
    fileMO.close();
    fileAB.close();
    fileGA.close();
    fileEN.close();
    //fileMO1.close();
    
}

//==================================================
//MONOPOLES=========================================
//==================================================

void LSLsimulation::Monopoles(double t)
{
  Site x(lattice);
  bool    lowEdge;
  double corner[8][3],norm;
  double pki1[3], proki[3], pki[3];
  FILE   *fout,*check,*filestr;
  char   nameout0[64],nameout1[64],nameout2[64],nameout3[64];
  fstream file0,file1,file2,file3;
  string  filename0,filename1,filename2,filename3;
  double ph1r,ph1i,ph2r,ph2i;

  sprintf(nameout0,"%f-monopolo%d_",t,parallel.rank()); 
  
  filename0 =path+nameout0+id+".dat" ;

  file0.open(filename0.c_str(), fstream::out | fstream::app);



  for(x.first(); x.test(); x.next() )
    {
	ph1r=phi1(x).real();
	ph1i=phi1(x).imag();
	ph2r=phi2(x).real();
	ph2i=phi2(x).imag();

      ki1(x,1)=2.0*(ph2r*ph1r+ph2i*ph1i);
      ki1(x,2)=2.0*(ph2i*ph1r-ph2r*ph1i);
      ki1(x,0)=phi1(x).norm()-phi2(x).norm();
    }

  ki1.updateHalo();

  for(x.first(); x.test(); x.next() )
    {
      roki(x,0)=ki1(x,2)-ki1(x-1,2)-ki1(x,1)+ki1(x-2,1);
      roki(x,1)=ki1(x,0)-ki1(x-2,0)-ki1(x,2)+ki1(x-0,2);
      roki(x,2)=ki1(x,1)-ki1(x-0,1)-ki1(x,0)+ki1(x-1,0);
    }

  roki.updateHalo();

  for(x.first(); x.test(); x.next() )
    {
      ki(x,0)=roki(x,2)-roki(x-1,2)-roki(x,1)+roki(x-2,1);
      ki(x,1)=roki(x,0)-roki(x-2,0)-roki(x,2)+roki(x-0,2);
      ki(x,2)=roki(x,1)-roki(x-0,1)-roki(x,0)+roki(x-1,0);
    }

  ki.updateHalo();
 

  for(x.first(); x.test(); x.next() )
    {
      double are=0.0;
      for(int g=0;g<3;g++)
	{
	  corner[0][g]=ki1(x,g)-ki(x,g);
	  corner[1][g]=ki1(x+1,g)-ki(x+1,g);
	  corner[2][g]=ki1(x+0,g)-ki(x+0,g);
	  corner[3][g]=ki1(x+0+1,g)-ki(x+0+1,g);
	  corner[4][g]=ki1(x+2,g)-ki(x+2,g);
	  corner[5][g]=ki1(x+1+2,g)-ki(x+1+2,g);
	  corner[6][g]=ki1(x+0+2,g)-ki(x+0+2,g);
	  corner[7][g]=ki1(x+0+1+2,g)-ki(x+0+1+2,g);
	}
	
	
      are+=computearea(corner[0],corner[1],corner[2]);/*1*/
      are+=computearea(corner[1],corner[3],corner[2]);/*2*/
      are+=computearea(corner[1],corner[5],corner[3]);/*3*/
      are+=computearea(corner[5],corner[7],corner[3]);/*4*/
      are+=computearea(corner[5],corner[4],corner[7]);/*5*/
      are+=computearea(corner[4],corner[6],corner[7]);/*6*/
      are+=computearea(corner[4],corner[0],corner[6]);/*7*/
      are+=computearea(corner[0],corner[2],corner[6]);/*8*/
      are+=computearea(corner[2],corner[3],corner[6]);/*9*/
      are+=computearea(corner[3],corner[7],corner[6]);/*10*/
      are+=computearea(corner[4],corner[5],corner[0]);/*11*/
      are+=computearea(corner[5],corner[1],corner[0]);/*12*/
      
      are/=(4*3.1418);
	
      if((are>0.9 and are<1.1) or (are>-1.1 and are<-0.9))file0<<x.coord(0)<<" "<<x.coord(1)<<" "<<x.coord(2)<<" "<<are<<"\n";

	}                                                                                                                          





  parallel.barrier();
  file0.close();  

}


//==================================================
//COMPUTE AREA======================================
//==================================================

double LSLsimulation::computearea(double a[3], double b[3], double c[3])
{

double aa, bb,cc,ss,area,moda,modb,modc;
double sign,alpha,beta,gama,abalpha,acbeta,bcgama;
double saa,sbb,scc;
double ab,bc,ac;
moda=sqrt(dot(a,a));
modb=sqrt(dot(b,b));
modc=sqrt(dot(c,c));
if(moda==0){moda=1.0;}
if(modb==0){modb=1.0;}
if(modc==0){modc=1.0;}

ab=acos(dot(a,b)/(moda*modb));
ac=acos(dot(a,c)/(moda*modc));
bc=acos(dot(b,c)/(modb*modc));



abalpha=(cos(ab)-cos(ac)*cos(bc))/(sin(ac)*sin(bc));

acbeta=(cos(ac)-cos(ab)*cos(bc))/(sin(ab)*sin(bc));

bcgama=(cos(bc)-cos(ab)*cos(ac))/(sin(ab)*sin(ac));

if(abalpha>1.0){abalpha=1.0;}
if(abalpha<-1.0){abalpha=-1.0;}
if(acbeta>1.0){acbeta=1.0;}
if(acbeta<-1.0){acbeta=-1.0;}
if(bcgama>1.0){bcgama=1.0;}
if(bcgama<-1.0){bcgama=-1.0;}



alpha=acos(abalpha);

beta=acos(acbeta);

gama=acos(bcgama);



area=alpha+beta+gama-3.14159265359;

sign=a[0]*b[1]*c[2]+b[0]*c[1]*a[2]+c[0]*a[1]*b[2]-a[2]*b[1]*c[0]-b[2]*c[1]*a[0]-c[2]*a[1]*b[0];

double sig;

sig=copysign(1.0,sign);


return(area*sig);


}

double LSLsimulation::computearea1(double a[3], double b[3], double c[3])
{
	double aa,bb,cc,ab,ac,bc,abc;
	double gamma,area;

	aa=dot(a,a);
	bb=dot(b,b);
	cc=dot(c,c);
	ab=dot(a,b);
	ac=dot(a,c);
	bc=dot(b,c);

	abc=a[1]*(b[2]*c[3]-b[3]*c[2])+a[2]*(b[3]*c[1]-b[1]*c[3])+a[3]*(b[1]*c[2]-b[2]*c[1]);

	gamma=(aa*bb*cc+ab*cc+ac*bb+bc*aa);


	area=2.0*atan2(abc,gamma);

	return(area);


}

//==================================================
//DOT===============================================
//==================================================

double LSLsimulation::dot(const double a[3], const double b[3])
{
int i;
double prod=0.0;

for(int i=0;i<3;i++){prod+=a[i]*b[i];}

return(prod);



}

//==================================================
//WINDING PLACKET===================================
//=================================================

int LSLsimulation::windingPlacket1(Site& x, int i, int j)
{
	Real phase[4];
	Real winding;
	
	phase[0]=acos( phi1(x).real() / sqrt(phi1(x).norm()) ); 
  	phase[1]=acos( phi1(x+i).real() / sqrt(phi1(x+i).norm()) ); 
  	phase[2]=acos( phi1(x+i+j).real() / sqrt(phi1(x+i+j).norm()) ); 
  	phase[3]=acos( phi1(x+j).real() / sqrt(phi1(x+j).norm()) ); 

  	if( phi1(x).imag()<0 )     { phase[0] *= -1; }
  	if( phi1(x+i).imag()<0 )   { phase[1] *= -1; }
  	if( phi1(x+i+j).imag()<0 ) { phase[2] *= -1; }
  	if( phi1(x+j).imag()<0 )   { phase[3] *= -1; }
  
  	winding  = moduloPi( phase[1] - phase[0] + theta(x,i) ) - theta(x,i);
  	winding += moduloPi( phase[2] - phase[1] + theta(x+i,j) ) - theta(x+i,j);
  	winding += moduloPi( phase[3] - phase[2] - theta(x+j,i) ) + theta(x+j,i);
  	winding += moduloPi( phase[0] - phase[3] - theta(x,j) ) + theta(x,j);
  
  	winding /= (2 * constants::pi);

  	return roundNearest(winding);

}

int LSLsimulation::windingPlacket2(Site& x, int i, int j)
{
        Real phase[4];
        Real winding;

        phase[0]=acos( phi2(x).real() / sqrt(phi2(x).norm()) );
        phase[1]=acos( phi2(x+i).real() / sqrt(phi2(x+i).norm()) );
        phase[2]=acos( phi2(x+i+j).real() / sqrt(phi2(x+i+j).norm()) );
        phase[3]=acos( phi2(x+j).real() / sqrt(phi2(x+j).norm()) );

        if( phi2(x).imag()<0 )     { phase[0] *= -1; }
        if( phi2(x+i).imag()<0 )   { phase[1] *= -1; }
        if( phi2(x+i+j).imag()<0 ) { phase[2] *= -1; }
        if( phi2(x+j).imag()<0 )   { phase[3] *= -1; }

        winding  = moduloPi( phase[1] - phase[0] + theta(x,i) ) - theta(x,i);
        winding += moduloPi( phase[2] - phase[1] + theta(x+i,j) ) - theta(x+i,j);
        winding += moduloPi( phase[3] - phase[2] - theta(x+j,i) ) + theta(x+j,i);
        winding += moduloPi( phase[0] - phase[3] - theta(x,j) ) + theta(x,j);

        winding /= (2 * constants::pi);

        return roundNearest(winding);

}

//==================================================
//EMT===============================================
//==================================================
void LSLsimulation::emTensorWinding(Site& x,double t, Real* T)
{
  Site x_0,x_1,x_2,x_0_1,x_0_2,x_1_2,x_0_1_2;
  
  x_0=x+0;
  x_1=x+1;
  x_2=x+2;
  x_0_1=x+0+1;
  x_0_2=x+0+2;
  x_1_2=x+1+2;
  x_0_1_2=x+0+1+2;
  
  Real a;
  Real tt;
 
  tt=(t+tinit);
  a=pow(tt,DAMP);
 
  Real aaL;
  Real E2;
  Real E2const,Econst;
  E2const = a*a * dx*dx;
  Econst= a*dx;
  Real B2;
  Real B2const,Bconst;
  B2const = a*a * dx*dx*dx*dx;
  Bconst =  a*dx*dx;
  Real EBconst = Econst*Bconst;
  Real D2const = dx*dx;
  Real Potconst = Beta * a * a / Real(4);
  
  Real Pi2;
  Real DiPhi2;
  
  Real Pot;
  Real ss = 1.0;
  
  Real E1B2,E2B1,E2B0,E0B2,E0B1,E1B0;
  Real B0B0,B1B1,B2B2;
  Real B2B0,B1B2,B0B1;
  Real E0E0,E1E1,E2E2;
  Real D0D0,D1D1,D2D2;
  Real E1E2,E2E0,E0E1;

  Imag D1D2,D2D0,D0D1;

  Imag piStarD0phi;
  Imag piStarD1phi;
  Imag piStarD2phi;
  
  Real B0_x,B0_x_0,B1_x, B1_x_1, B2_x, B2_x_2;
  
  Imag Dp0_x,Dp0_x_1,Dp0_x_2,Dp0_x_1_2;
  Imag Dp1_x,Dp1_x_0,Dp1_x_2,Dp1_x_0_2;
  Imag Dp2_x,Dp2_x_0,Dp2_x_1,Dp2_x_0_1;
  
  Imag Dm0_x_0,Dm0_x_0_1,Dm0_x_0_2,Dm0_x_0_1_2;
  Imag Dm1_x_1,Dm1_x_0_1,Dm1_x_1_2,Dm1_x_0_1_2;
  Imag Dm2_x_2,Dm2_x_0_2,Dm2_x_1_2,Dm2_x_0_1_2;
  
  // Electric field on links. Average over 4 links per direction.
  //E0E0
  E0E0 =  0.25*(epsilon(x,0)*epsilon(x,0)) +  (epsilon(x_1,0)*epsilon(x_1,0)) + (epsilon(x_2,0)*epsilon(x_2,0)) +(epsilon(x_1_2,0)*epsilon(x_1_2,0));
  E0E0 /= E2const;
  //E1E1
  E1E1 =  0.25*(epsilon(x,1)*epsilon(x,1)) +  (epsilon(x_0,1)*epsilon(x_0,1)) + (epsilon(x_2,1)*epsilon(x_2,1)) +(epsilon(x_0_2,1)*epsilon(x_0_2,1));
  E1E1 /= E2const;
  //E2E2
  E2E2 =  0.25*(epsilon(x,2)*epsilon(x,2)) +  (epsilon(x_0,2)*epsilon(x_0,2)) + (epsilon(x_1,2)*epsilon(x_1,2)) +(epsilon(x_0_1,2)*epsilon(x_0_1,2));
  E2E2 /= E2const;
  E2 = E0E0 + E1E1 + E2E2;
  
  // Magnetic field on plaquettes. Average over 2 plaquettes per direction.
  
  B0_x =   Real(2)*sin( (theta(x_1,2)   - theta(x,2)   - theta(x_2,1)   + theta(x,1))  / Real(2) );
  B0_x_0 = Real(2)*sin( (theta(x_0_1,2) - theta(x_0,2) - theta(x_0_2,1) + theta(x_0,1))/ Real(2) );
  B1_x =   Real(2)*sin( (theta(x_2,0)   - theta(x,0)   - theta(x_0,2)   + theta(x,2))  / Real(2) );
  B1_x_1 = Real(2)*sin( (theta(x_1_2,0) - theta(x_1,0) - theta(x_0_1,2) + theta(x_1,2))/ Real(2) );
  B2_x =   Real(2)*sin( (theta(x_0,1)   - theta(x,1)   - theta(x_1,0)   + theta(x,0))  / Real(2) );
  B2_x_2 = Real(2)*sin( (theta(x_0_2,1) - theta(x_2,1) - theta(x_1_2,0) + theta(x_2,0))/ Real(2) );
  
  //=B0B0
  B0B0 = 0.5*(B0_x*B0_x + B0_x_0*B0_x_0)/B2const;
  //=B1B1
  B1B1 = 0.5*(B1_x*B1_x + B1_x_1*B1_x_1)/B2const;
  //=B2B2
  B2B2 = 0.5*(B2_x*B2_x + B2_x_2*B2_x_2)/B2const;
  
  B2 =  B0B0 + B1B1 + B2B2; 
  
  // Canonical momentum and potential. Average over 8 sites in the cell.
  Pi2 = pi1(x).norm() + pi2(x).norm() + pi1(x_0).norm() + pi2(x_0).norm() + pi1(x_1).norm() + pi2(x_1).norm() + pi1(x_2).norm() + pi2(x_2).norm() + pi1(x_0_1).norm() + pi2(x_0_1).norm() + pi1(x_0_2).norm() + pi2(x_0_2).norm() + pi1(x_1_2).norm() + pi2(x_1_2).norm() + pi1(x_0_1_2).norm() + pi2(x_0_1_2).norm();
  Pi2/=Real(8);
  
  Pot =  pow(phi1(x).norm()+phi2(x).norm()-ss, Real(2))     + pow(phi1(x_0).norm()+phi2(x_0).norm()-ss, Real(2)) \
  +      pow(phi1(x_1).norm()+phi2(x_1).norm()-ss, Real(2))   + pow(phi1(x_2).norm()+phi2(x_2).norm()-ss, Real(2)) \
  +      pow(phi1(x_0_1).norm()+phi2(x_0_1).norm()-ss, Real(2)) + pow(phi1(x_0_2).norm()+phi2(x_0_2).norm()-ss, Real(2)) \
  +      pow(phi1(x_1_2).norm()+phi2(x_1_2).norm()-ss, Real(2)) + pow(phi1(x_0_1_2).norm()+phi2(x_0_1_2).norm()-ss, Real(2));
  Pot /= Real(8);  //Average
  Pot *= Potconst; //Normalisation
  
  // Scalar gradients. Average over 4 links per direction and forward (p) and backward (m) derivatives.  
  // carefull D is not define with /dx, /dx need to be performed afterward
  
  Dp0_x     = phi1(x_0)    *expi(theta(x,0))    -phi1(x)    +phi2(x_0)    *expi(theta(x,0))    -phi2(x);
  Dp0_x_1   = phi1(x_0_1)  *expi(theta(x_1,0))  -phi1(x_1)  +phi2(x_0_1)  *expi(theta(x_1,0))  -phi2(x_1);
  Dp0_x_2   = phi1(x_0_2)  *expi(theta(x_2,0))  -phi1(x_2)  +phi2(x_0_2)  *expi(theta(x_2,0))  -phi2(x_2);
  Dp0_x_1_2 = phi1(x_0_1_2)*expi(theta(x_1_2,0))-phi1(x_1_2)+phi2(x_0_1_2)*expi(theta(x_1_2,0))-phi2(x_1_2);
  Dp1_x     = phi1(x_1)    *expi(theta(x,1))    -phi1(x)    +phi2(x_1)    *expi(theta(x,1))    -phi2(x);
  Dp1_x_0   = phi1(x_0_1)  *expi(theta(x_0,1))  -phi1(x_0)  +phi2(x_0_1)  *expi(theta(x_0,1))  -phi2(x_0);
  Dp1_x_2   = phi1(x_1_2)  *expi(theta(x_2,1))  -phi1(x_2)  +phi2(x_1_2)  *expi(theta(x_2,1))  -phi2(x_2);
  Dp1_x_0_2 = phi1(x_0_1_2)*expi(theta(x_0_2,1))-phi1(x_0_2)+phi2(x_0_1_2)*expi(theta(x_0_2,1))-phi2(x_0_2);
  Dp2_x     = phi1(x_2)    *expi(theta(x,2))    -phi1(x)    +phi2(x_2)    *expi(theta(x,2))    -phi2(x);
  Dp2_x_0   = phi1(x_0_2)  *expi(theta(x_0,2))  -phi1(x_0)  +phi2(x_0_2)  *expi(theta(x_0,2))  -phi2(x_0);
  Dp2_x_1   = phi1(x_1_2)  *expi(theta(x_1,2))  -phi1(x_1)  +phi2(x_1_2)  *expi(theta(x_1,2))  -phi2(x_1);
  Dp2_x_0_1 = phi1(x_0_1_2)*expi(theta(x_0_1,2))-phi1(x_0_1)+phi2(x_0_1_2)*expi(theta(x_0_1,2))-phi2(x_0_1);
  
  Dm0_x_0     = phi1(x_0)     -expi(-theta(x,0))    *phi1(x)    +phi2(x_0)     -expi(-theta(x,0))    *phi2(x);
  Dm0_x_0_1   = phi1(x_0_1)   -expi(-theta(x_1,0))  *phi1(x_1)  +phi2(x_0_1)   -expi(-theta(x_1,0))  *phi2(x_1);
  Dm0_x_0_2   = phi1(x_0_2)   -expi(-theta(x_2,0))  *phi1(x_2)  +phi2(x_0_2)   -expi(-theta(x_2,0))  *phi2(x_2);
  Dm0_x_0_1_2 = phi1(x_0_1_2) -expi(-theta(x_1_2,0))*phi1(x_1_2)+phi2(x_0_1_2) -expi(-theta(x_1_2,0))*phi2(x_1_2);
  Dm1_x_1     = phi1(x_1)     -expi(-theta(x,1))    *phi1(x)    +phi2(x_1)     -expi(-theta(x,1))    *phi2(x);
  Dm1_x_0_1   = phi1(x_0_1)   -expi(-theta(x_0,1))  *phi1(x_0)  +phi2(x_0_1)   -expi(-theta(x_0,1))  *phi2(x_0);
  Dm1_x_1_2   = phi1(x_1_2)   -expi(-theta(x_2,1))  *phi1(x_2)  +phi2(x_1_2)   -expi(-theta(x_2,1))  *phi2(x_2);
  Dm1_x_0_1_2 = phi1(x_0_1_2) -expi(-theta(x_0_2,1))*phi1(x_0_2)+phi2(x_0_1_2) -expi(-theta(x_0_2,1))*phi2(x_0_2);
  Dm2_x_2     = phi1(x_2)     -expi(-theta(x,2))    *phi1(x)    +phi2(x_2)     -expi(-theta(x,2))    *phi2(x);
  Dm2_x_0_2   = phi1(x_0_2)   -expi(-theta(x_0,2))  *phi1(x_0)  +phi2(x_0_2)   -expi(-theta(x_0,2))  *phi2(x_0);
  Dm2_x_1_2   = phi1(x_1_2)   -expi(-theta(x_1,2))  *phi1(x_1)  +phi2(x_1_2)   -expi(-theta(x_1,2))  *phi2(x_1);
  Dm2_x_0_1_2 = phi1(x_0_1_2) -expi(-theta(x_0_1,2))*phi1(x_0_1)+phi2(x_0_1_2) -expi(-theta(x_0_1,2))*phi2(x_0_1);
  
  
  D0D0  = (Dp0_x.norm() + Dp0_x_1.norm() + Dp0_x_2.norm() + Dp0_x_1_2.norm())/(D2const*Real(4));
  D1D1  = (Dp1_x.norm() + Dp1_x_0.norm() + Dp1_x_2.norm() + Dp1_x_0_2.norm())/(D2const*Real(4));
  D2D2  = (Dp2_x.norm() + Dp2_x_0.norm() + Dp2_x_1.norm() + Dp2_x_0_1.norm())/(D2const*Real(4));
  // Note |forward|^2 = |backward|^2 so don't need to average over p and m
  DiPhi2 = D0D0 + D1D1 + D2D2;     

  // Now field products for T0i
  E1B2 =  (epsilon(x,1)   + epsilon(x_0,1))   * B2_x;
  E1B2 += (epsilon(x_2,1) + epsilon(x_0_2,1)) * B2_x_2;
  E1B2 /= Real(4); //Average 
  E1B2 /=EBconst;  //Normalisation
  
  E2B1 =  (epsilon(x,2)   + epsilon(x_0,2))   * B1_x;
  E2B1 += (epsilon(x_1,2) + epsilon(x_0_1,2)) * B1_x_1;
  E2B1 /=Real(4);
  E2B1 /=EBconst;
  
  E2B0 =  (epsilon(x,2)   + epsilon(x_1,2))   * B0_x;
  E2B0 += (epsilon(x_0,2) + epsilon(x_0_1,2)) * B0_x_0;
  E2B0 /=Real(4);
  E2B0 /=EBconst;
  
  E0B2 =  (epsilon(x,0)   + epsilon(x_1,0))   * B2_x;
  E0B2 += (epsilon(x_2,0) + epsilon(x_1_2,0)) * B2_x_2 ;
  E0B2 /=Real(4);
  E0B2 /=EBconst;
  
  E0B1 =  (epsilon(x,0)   + epsilon(x_2,0))   * B1_x ;
  E0B1 += (epsilon(x_1,0) + epsilon(x_1_2,0)) * B1_x_1;
  E0B1 /=Real(4);
  E0B1 /=EBconst;
  
  E1B0 =  (epsilon(x,1)   + epsilon(x_2,1))   * B0_x ;
  E1B0 += (epsilon(x_0,1) + epsilon(x_0_2,1)) * B0_x_0 ;
  E1B0 /= Real(4);
  E1B0 /= EBconst;
  
  piStarD0phi =
  pi1(x).conj()     * Dp0_x     + pi1(x+0).conj()     * Dm0_x_0   + \
  pi1(x+1).conj()   * Dp0_x_1   + pi1(x+0+1).conj()   * Dm0_x_0_1 + \
  pi1(x+2).conj()   * Dp0_x_2   + pi1(x+0+2).conj()   * Dm0_x_0_2 + \
  pi1(x+1+2).conj() * Dp0_x_1_2 + pi1(x+0+1+2).conj() * Dm0_x_0_1_2 + \
  pi2(x).conj()     * Dp0_x     + pi2(x+0).conj()     * Dm0_x_0   + \
  pi2(x+1).conj()   * Dp0_x_1   + pi2(x+0+1).conj()   * Dm0_x_0_1 + \
  pi2(x+2).conj()   * Dp0_x_2   + pi2(x+0+2).conj()   * Dm0_x_0_2 + \
  pi2(x+1+2).conj() * Dp0_x_1_2 + pi2(x+0+1+2).conj() * Dm0_x_0_1_2;
  piStarD0phi /= (Real(8)*dx);
  
  piStarD1phi =
  pi1(x).conj()     * Dp1_x     + pi1(x+1).conj()     * Dm1_x_1   + \
  pi1(x+0).conj()   * Dp1_x_0   + pi1(x+1+0).conj()   * Dm1_x_0_1 + \
  pi1(x+2).conj()   * Dp1_x_2   + pi1(x+1+2).conj()   * Dm1_x_1_2 + \
  pi1(x+0+2).conj() * Dp1_x_0_2 + pi1(x+1+0+2).conj() * Dm1_x_0_1_2+ \
  pi2(x).conj()     * Dp1_x     + pi2(x+1).conj()     * Dm1_x_1   + \
  pi2(x+0).conj()   * Dp1_x_0   + pi2(x+1+0).conj()   * Dm1_x_0_1 + \
  pi2(x+2).conj()   * Dp1_x_2   + pi2(x+1+2).conj()   * Dm1_x_1_2 + \
  pi2(x+0+2).conj() * Dp1_x_0_2 + pi2(x+1+0+2).conj() * Dm1_x_0_1_2 ;
  piStarD1phi /= (Real(8)*dx);
  
  piStarD2phi =
  pi1(x).conj()     * Dp2_x     + pi1(x+2).conj()     * Dm2_x_2   + \
  pi1(x+0).conj()   * Dp2_x_0   + pi1(x+2+0).conj()   * Dm2_x_0_2 + \
  pi1(x+1).conj()   * Dp2_x_1   + pi1(x+2+1).conj()   * Dm2_x_1_2 + \
  pi1(x+0+1).conj() * Dp2_x_0_1 + pi1(x+2+1+0).conj() * Dm2_x_0_1_2 + \
  pi2(x).conj()     * Dp2_x     + pi2(x+2).conj()     * Dm2_x_2   + \
  pi2(x+0).conj()   * Dp2_x_0   + pi2(x+2+0).conj()   * Dm2_x_0_2 + \
  pi2(x+1).conj()   * Dp2_x_1   + pi2(x+2+1).conj()   * Dm2_x_1_2 + \
  pi2(x+0+1).conj() * Dp2_x_0_1 + pi2(x+2+1+0).conj() * Dm2_x_0_1_2;
  piStarD2phi /= (Real(8)*dx);
  
  
  // Now field products for Tij
  
  E1E2 = (epsilon(x,1)+epsilon(x_0,1)) * (epsilon(x,2)+epsilon(x_0,2));
  E1E2 /= Real(4) * E2const;
  
  E2E0 = (epsilon(x,2)+epsilon(x_1,2)) * (epsilon(x,0)+epsilon(x_1,0));
  E2E0 /= Real(4) * E2const;
  
  E0E1 = (epsilon(x,0)+epsilon(x_2,0)) * (epsilon(x,1)+epsilon(x_2,1));;
  E0E1 /= Real(4) * E2const;
  
  B1B2 = (B1_x + B1_x_1)*(B2_x + B2_x_2);
  B1B2 /= Real(4) * B2const;
  
  B2B0 = (B2_x + B2_x_2)*(B0_x + B0_x_0);
  B2B0 /= Real(4) * B2const;
  
  B0B1 = (B0_x + B0_x_0)*(B1_x + B1_x_1);
  B0B1 /= Real(4) * B2const;
  
  D1D2 =  Dp1_x.conj()  *Dp2_x   + Dp1_x_2.conj()  *Dm2_x_2   + Dm1_x_1.conj()  *Dp2_x_1   + Dm1_x_1_2.conj()  *Dm2_x_1_2;
  D1D2 += Dp1_x_0.conj()*Dp2_x_0 + Dp1_x_0_2.conj()*Dm2_x_0_2 + Dm1_x_0_1.conj()*Dp2_x_0_1 + Dm1_x_0_1_2.conj()*Dm2_x_0_1_2;
  D1D2 /= Real(8)*D2const;
  
  D2D0 =  Dp2_x.conj()  *Dp0_x   + Dp2_x_0.conj()  *Dm0_x_0   + Dm2_x_2.conj()  *Dp0_x_2   + Dm2_x_0_2.conj()  *Dm0_x_0_2;
  D2D0 += Dp2_x_1.conj()*Dp0_x_1 + Dp2_x_0_1.conj()*Dm0_x_0_1 + Dm2_x_1_2.conj()*Dp0_x_1_2 + Dm2_x_0_1_2.conj()*Dm0_x_0_1_2;
  D2D0 /= Real(8)*D2const;
  
  D0D1 =  Dp0_x.conj()  *Dp1_x   + Dp0_x_1.conj()  *Dm1_x_1   + Dm0_x_0.conj()  *Dp1_x_0   + Dm0_x_0_1.conj()  *Dm1_x_0_1;
  D0D1 += Dp0_x_2.conj()*Dp1_x_2 + Dp0_x_1_2.conj()*Dm1_x_1_2 + Dm0_x_0_2.conj()*Dp1_x_0_2 + Dm0_x_0_1_2.conj()*Dm1_x_0_1_2;
  D0D1 /= Real(8)*D2const;
  
  // Now we can start working out EM tensor (downstairs indices)
  
  // Lagrangian density * scale factor squared
  // (a^2 L) = (1/2) sum_j F0jF0j/a^2 - (1/4) sum_ij FijFij/a^2 + |Pi|^2 - |DiPhi|^2 - (a^2 V) 
  aaL = 0.5*(E2-B2)+Pi2-DiPhi2-Pot;
  
  // Energy density T00 = sum_j F0jF0j/a^2 + 2|Pi|^2 - (a^2 L)
  
  // T(0,0)
  T[0]= E2 + Real(2)*Pi2-aaL;
  
  // Momentum density T0i = sum_j F0jFij/a^2 + (Pi*)(DiPhi) + c.c.
  
  // T(0,x) 
  T[1]= E1B2 - E2B1 + Real(2)*piStarD0phi.real();
  // T(0,y) 
  T[2]= E2B0 - E0B2 + Real(2)*piStarD1phi.real();
  // T(0,z) 
  T[3]= E0B1 - E1B0 + Real(2)*piStarD2phi.real();
  
  // Diagonal Tii = sum_k FikFik/a^2 - Fi0Fi0/a^2 + (DiPhi*)(DiPhi) + c.c. + (a^2 L) (no sum on i)
  
  //T(x,x) 
  T[4] = - E0E0 + B1B1 + B2B2 + Real(2)*D0D0 + aaL ;
  //T(y,y)
  T[7] = - E1E1 + B0B0 + B2B2 + Real(2)*D1D1 + aaL ;
  //T(z,z) 
  T[9] = B2;//- E2E2 + B0B0 + B1B1 + Real(2)*D2D2 + aaL ;
  
  // Off-diagonal Tij = FikFjk/a^2 - Fi0Fj0/a^2 + (DiPhi*)(DjPhi) + c.c. (only one k in sum)
  
  //T(x,y)
  T[5] = - E0E1 - B0B1 + Real(2)*D0D1.real() ;
  //T(x,z)
  T[6] = - E2E0 - B2B0 + Real(2)*D2D0.real() ;
  //T(y,z)
  T[8] = Pot;//- E1E2 - B1B2 + Real(2)*D1D2.real() ;
  
}

//==================================================
//ABIADURAK=========================================
//==================================================

void LSLsimulation::abiadurak(double t)
{

Site x(lattice);

int i,j;
char   nameout0[64];

double e22,b2;

double la,Fij, f0i,po,pipi,diphi,diphi1;

double lae2,lab2,lapipi,ladiphi,tola,tolae2,tolab2,tolapipi,toladiphi;

double e2l,b2l,pipil,diphil,v,v1;

double veb,vpd;

fstream file0;
  string  filename0;

  sprintf(nameout0,"abiadurak_");
  
  filename0 = path+nameout0+id+".dat" ;
  


file0.open(filename0.c_str(), fstream::out | fstream::app);

tola=0.0;

tolae2=0.0;

tolab2=0.0;

tolapipi=0.0;

toladiphi=0.0;

 for( x.first(); x.test(); x.next()) 
   {

     b2=0.0;
     f0i=0.0;
     Fij=0.0;
     e22=0.0;
     diphi=0.0;
     pipi=0.0;     
     po=0.0;	

     for(i=0; i<dim; i++) 
       {

	 f0i+=epsilon(x,i)*epsilon(x,i);
	 diphi+=((expi(-dx*theta(x,i))*phi1(x+i)-phi1(x))/dx).norm()+((expi(-dx*theta(x,i))*phi2(x+i)-phi2(x))/dx).norm(); 
	 for(int j=(i==0); j<dim;j++,j+=(i==j)) 
	   {
             Fij+=(1.0-cos(dx*(theta(x,j)+theta(x+j,i)-theta(x+i,j)-theta(x,i))));
	   }
       }


     po=Beta*(phi1(x).norm()+phi2(x).norm()-1.0)*(phi1(x).norm()+phi2(x).norm()-1.0);

     pipi=pi1(x).norm()+pi2(x).norm();

     Fij/=dx*dx*dx*dx; 

     la=abs(pipi-0.5*f0i-0.5*po-diphi-0.5*Fij);

     lae2=f0i*la;

     lab2=2*Fij*la;

     lapipi=pipi*la;

     ladiphi=diphi*la;

     tola+=la;

     tolae2+=lae2;

     tolab2+=lab2;

     tolapipi+=lapipi;

     toladiphi+=ladiphi;

   }



parallel.sum(tola);

parallel.sum(tolae2);

parallel.sum(tolab2);

parallel.sum(tolapipi);

parallel.sum(toladiphi);


e2l=tolae2/tola;
b2l=tolab2/tola;
pipil=tolapipi/tola;
diphil=toladiphi/tola;

v=e2l/b2l;

veb=v; 

v1=pipil/diphil;

 vpd=2*v1/(1+v1); 

 if(parallel.isRoot()){file0<<t<<" "<<veb<<" "<<vpd<<"\n";}

file0.close();

}







//==================================================
//GAUSS LAW=========================================
//==================================================


void LSLsimulation::GaussTest(double t)
{

Site x(lattice);

Real divEpsilon;
double  sumSqPiConjPhiImag = 0.0;
double  sumSqDivEpsilon = 0.0;
Real    qGauss;
bool    lowEdge;
 string filename;
 fstream file;
 char nameout[64];



 filename=path+"gauss_"+id+".dat";

 file.open(filename.c_str(), fstream::out | fstream::app);


for( x.first(); x.test(); x.next() )
   {
     lowEdge=0;
     for(int i=0; i<dim; i++){if(x.coordLocal(i)==0){lowEdge = 1;}}
     if(!lowEdge)
	{
	sumSqPiConjPhiImag += pow((pi1(x).conj()*phi1(x)+pi2(x).conj()*phi2(x)).imag(),Real(2));
	divEpsilon=Real(0);
	for(int i=0; i<dim; i++) {divEpsilon += epsilon(x,i) - epsilon(x-i,i);}
	sumSqDivEpsilon += divEpsilon * divEpsilon;
	}
    }
  sumSqDivEpsilon /= dx*dx;

  parallel.sum(sumSqPiConjPhiImag);
  parallel.sum(sumSqDivEpsilon);


  if(parallel.isRoot()){file<<t<<" "<<2.0*sqrt(sumSqPiConjPhiImag/lattice.sites())<<" "<<sqrt(sumSqDivEpsilon/lattice.sites())<<endl;}

  }

//=================================================
//ENERGIA==========================================
//=================================================

void LSLsimulation::energia(double t)
{

Site x(lattice);
double sumF0i=Real(0);
double sumFij=Real(0);
double sumPi=Real(0);
double sumDiPhi=Real(0);
double sumV=Real(0);
bool    lowEdge;
char   nameout0[64];

fstream file0;
  string  filename0;

  sprintf(nameout0,"energia1_");
  
  filename0 = path+nameout0+id+".dat" ;
  


file0.open(filename0.c_str(), fstream::out | fstream::app);


for(x.first(); x.test(); x.next())
	{

	
	  sumPi += pi1(x).norm()+pi2(x).norm();
	  sumV +=(phi1(x).norm()+phi2(x).norm()-1.0)*(phi1(x).norm()+phi2(x).norm()-1.0);
		
		for(int i=0; i<dim; i++)
			{
			  sumF0i +=epsilon(x,i)*epsilon(x,i);
			  sumDiPhi += ((expi(dx*theta(x,i))*phi1(x+i)-phi1(x))/dx).norm()+((expi(dx*theta(x,i))*phi2(x+i)-phi2(x))/dx).norm();
			for(int j=(i==0); j<dim;j++,j+=(i==j))
				{
				  sumFij += 1.0-cos(dx*(theta(x,j)+theta(x+j,i)-theta(x+i,j)-theta(x,i)));
				}
			}
	  
	}

sumF0i /=Real(2);
sumFij /= Real(2)*dx*dx*dx*dx;
sumV *=0.5*Beta;

parallel.sum(sumF0i);
parallel.sum(sumFij);
parallel.sum(sumPi);
parallel.sum(sumDiPhi);
parallel.sum(sumV);



 if(parallel.isRoot()){file0<<t<<" "<<sumF0i/lattice.sites()<<" "<<sumFij/lattice.sites()<<" "<<sumPi/lattice.sites()<<" "<<sumDiPhi/lattice.sites()<<" "<<sumV/lattice.sites()<<" "<<sumF0i/lattice.sites()+sumFij/lattice.sites()+sumPi/lattice.sites()+sumDiPhi/lattice.sites()+sumV/lattice.sites()<<"\n";}

}






















void LSLsimulation::hibernateSave(){}
void LSLsimulation::hibernateLoad(){}
void LSLsimulation::statsFileLine(){}

