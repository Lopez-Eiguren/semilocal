#ifndef XMATH_HPP
#define XMATH_HPP

#include <cmath>
#include "constants.hpp"

int roundUp(double input)
{
  return int(ceil(input));
}

int roundDown(double input)
{
  return int(floor(input));
}

int roundNearest(double input)
{
  double f=floor(input);
  if(input-f<0.5) { return int(f); }
  else { return int(f)+1; }
}

bool delta(int i, int j)
{
  return i==j;
}

double moduloPi(double x)
{
  if( x>=constants::pi )
    {
      return x-2*constants::pi*floor( double( (constants::pi+x) / (2*constants::pi) ) );
    }
  else if( x<-constants::pi )
    {
      return x+2*constants::pi*floor( double( (constants::pi+abs(x)) / (2*constants::pi) ) );
    }
  else return x;
}

#endif
