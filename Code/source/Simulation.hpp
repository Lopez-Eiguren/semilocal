#ifndef SIMULATION_HPP
#define SIMULATION_HPP

#include <sstream>




class Simulation
{
public:
  //Constructor
  Simulation();

  //Energy-momentum tensor operations
  void emAlloc();  
  void emDealloc();

  //Energy-momentum conversation operations
  void emConservationInit(string preString, string postString);
  void emConservationCommon();
  void emConservationUpdatePre();
  void emConservationUpdate();
  void emConservationUpdatePost();

  //Statistics file functions
  void statsFileInit(string preString, string postString);
  void statsFileUpdate();
  virtual void statsFileLine() = 0;

  //Hibernation functions
  void hibernate();
  void hibernateInitialize(bool wake, string preString, string postString);
  void hibernateWake();
  bool hibernateWaking() { return hibernateWaking_; };
  virtual void hibernateSave() = 0;
  virtual void hibernateLoad() = 0;

public:

  //Generic lattice field evolution variables
  int  dim; //Number of spatial dimensions
  int  N;   //Number of lattice points in each direction
  Real dx;  //Lattice spacing
  Real dt;  //Time increment per timestep
  Real tStart; 
  Real tEnd;
  Real t;    
  int timeStep;    //Time step number
  int timeStep2;   //Second time step count (used eg. for extra diffusive steps between timesteps)

  //Generic cosmological variables
  Real adot_a;     //For emConverationUpdate()

  //Lattice
  Lattice lattice;

  //Energy-momentum tensor
  Lattice     emLattice;        //Zero halo!
  Field<Real> emTensor;

private:
  //Energy-momentum tensor conservation
  int       emConservationStatus_;
  string    emConservationFileName_;
  ofstream  emConservationFile_;

  //Statistics file variables
  int       statsFileStatus_;
  string    statsFileName_;

  //Hibernation variables
  bool hibernateWaking_;

protected:
  //Statistis file variables
  ofstream  statsFile_;

  //Hibernation variables
  fstream hibernateFile_;
  string hibernatePreString_;
  string hibernatePostString_;
};

//CONSTRUCTOR=============================

Simulation::Simulation() 
{
  emConservationStatus_=0;
  statsFileStatus_=0;
  hibernateWaking_ = 0;
}


//ENERGY-MOMENTUM TENSOR OPERATIONS=======

void Simulation::emAlloc()
{
  emLattice.initialize(dim, N, 0);
  emTensor.initialize(emLattice, 4, 4, LATfield2d::symmetric);
  emTensor.alloc();
}

void Simulation::emDealloc()
{
  emTensor.dealloc();
}

//ENERGY-MOMENTUM CONSERVATION OPERATIONS=====

void Simulation::emConservationInit(string preString, string postString)
{
  if( parallel.isRoot() )
    {
      emConservationFileName_=preString+"emConserve"+postString;

      if(hibernateWaking_==0)
	{
	  emConservationFile_.open( emConservationFileName_.c_str(), fstream::out | fstream::trunc);
	}
      else
	{
	  emConservationFile_.open( emConservationFileName_.c_str(), fstream::out | fstream::app);
	}
      
      if(!emConservationFile_.good())
	{
	  cout<<"Simulation::emConvservationInit() - Could not open file: "<<emConservationFileName_<<endl;
	  cout<<"Simulation::emConvservationInit() - Exiting..."<<endl;
	  parallel.abortRequest();
	}
      
      if(hibernateWaking_==0)
	{
	  emConservationFile_<<"#Energy-momentum conservation test"<<endl;
	  emConservationFile_<<"#Column 1: Time of Pre-step"<<endl;
	  emConservationFile_<<"#Column 2: Average EM-density at Pre-step"<<endl;
	  emConservationFile_<<"#Column 3: Time of Mid-step"<<endl;
	  emConservationFile_<<"#Column 4: adot-over-a at Mid-step"<<endl;
	  emConservationFile_<<"#Column 5: Average EM-density at mid-step"<<endl;
	  emConservationFile_<<"#Column 6: Average 3*pressure mid-step"<<endl;
	  emConservationFile_<<"#Column 7: Time of Post-step"<<endl;
	  emConservationFile_<<"#Column 8: Average EM-desnity at Post-step"<<endl;
	}

      emConservationFile_.close();  

     
      //Set status
      emConservationStatus_=1;
    }
  parallel.barrier();
}

void Simulation::emConservationCommon()
{
  emConservationFile_.open( emConservationFileName_.c_str(), fstream::out | fstream::app);
  
  if(!emConservationFile_.good())
    {
      cout<<"Simulation::emConvservationCommon() - Could not open file: "<<emConservationFileName_<<endl;
      cout<<"Simulation::emConvservationCommon() - Exiting..."<<endl;
      exit(EXIT_FAILURE);
    }
  
  emConservationFile_.precision(7);
  emConservationFile_.width(emConservationFile_.precision()+7);
  emConservationFile_.setf(fstream::scientific | fstream::showpos);
}

void Simulation::emConservationUpdatePre()
{
  if(parallel.isRoot())
    {
      //Check status
      if(emConservationStatus_!=1)
	{
	  COUT<<"Simulation::emConvservationUpdatePre() - object not is ready state for this function"<<endl;
	  COUT<<"Simulation::emConvservationUpdatePre() - Exiting..."<<endl;
	  parallel.abortRequest();
	}
      
      this->emConservationCommon();

      //Write Pre time
      emConservationFile_<<t<<" ";
    }
  parallel.barrier();

  //Sum current energy-density
  Site x(emLattice);
  Real T00Sum=0.0;

  for(x.first(); x.test(); x.next())
    {
      T00Sum += emTensor(x,0,0);
    }
  parallel.sum(T00Sum);

  if(parallel.isRoot())
    {
      //Write average energy density
      emConservationFile_<<T00Sum/pow(N, Real(dim))<<" ";

      emConservationFile_.close();
    }

  //Set status
  emConservationStatus_=2;
}

void Simulation::emConservationUpdate()
{
  //Check status
  if(emConservationStatus_!=2)
    {
      COUT<<"Simulation::emConvservationUpdate() - object not is ready state for this function"<<endl;
      COUT<<"Simulation::emConvservationUpdate() - Exiting..."<<endl;
      exit(EXIT_FAILURE);
    }

  if(parallel.isRoot())
    {
      this->emConservationCommon();

      //Write time and adot_a
      emConservationFile_<<t<<" "<<adot_a<<" ";
    }

  //Sum current energy-density and pressure
  Site x(emLattice);
  Real T00Sum=0.0;
  Real TjjSum=0.0;

  for(x.first(); x.test(); x.next())
    {
      T00Sum += emTensor(x,0,0);
      for(int j=0; j<dim; j++) { TjjSum += emTensor(x,j+1,j+1); }
    }
  parallel.sum(T00Sum);
  parallel.sum(TjjSum);

  if(parallel.isRoot())
    {
      //Write averages
      emConservationFile_<<T00Sum/pow(N, Real(dim))<<" "<<TjjSum/pow(N, Real(dim))<<" ";

      emConservationFile_.close();
    }

  //Set status
  emConservationStatus_=3;
}

void Simulation::emConservationUpdatePost()
{
  //Check status
  if(emConservationStatus_!=3)
    {
      COUT<<"Simulation::emConvservationUpdatePost() - object not is ready state for this function"<<endl;
      COUT<<"Simulation::emConvservationUpdatePost() - Exiting..."<<endl;
      exit(EXIT_FAILURE);
    }

   if(parallel.isRoot())
    {
      this->emConservationCommon();

      //Write Post time
      emConservationFile_<<t<<" ";
    }

   //Sum current energy-density
  Site x(emLattice);
  Real T00Sum=0.0;

  for(x.first(); x.test(); x.next())
    {
      T00Sum += emTensor(x,0,0);
    }
  parallel.sum(T00Sum);

  if(parallel.isRoot())
    {
      //Write average energy density
      emConservationFile_<<T00Sum/pow(N, Real(dim))<<endl;
      
      emConservationFile_.close();
    }

  //Set status
  emConservationStatus_=1;
}


//STATSISTICS FILE FUNCTIONS===========================

void Simulation::statsFileInit(string preString, string postString)
{
  if( parallel.isRoot() )
    {
      statsFileName_=preString+"statsFile"+postString;

      if(hibernateWaking_==0)
	{
	  statsFile_.open( statsFileName_.c_str(), fstream::out | fstream::trunc);
	}
      else
	{
	  statsFile_.open( statsFileName_.c_str(), fstream::out | fstream::app);
	}

      if(!statsFile_.good())
	{
	  cout<<"Simulation::statsFileInit() - Could not open file: "<<statsFileName_<<endl;
	  cout<<"Simulation::statsFileInit() - Exiting..."<<endl;
	  parallel.abortRequest();
	}
      
      statsFile_.close();  
    }
  parallel.barrier();

  //Set status
  statsFileStatus_=1;
}

void Simulation::statsFileUpdate()
{
  if( parallel.isRoot() )
    {
      //Check status
      if(statsFileStatus_!=1)
	{
	  cout<<"Simulation::statsFileUpdate() - object not is ready state for this function"<<endl;
	  cout<<"Simulation::statsFileUpdate() - Exiting..."<<endl;
	  parallel.abortRequest();
	}

      statsFile_.open( statsFileName_.c_str(), fstream::out | fstream::app);
      
      if(!statsFile_.good())
	{
	  cout<<"Simulation::statsFileUpdate() - Could not open file: "<<statsFileName_<<endl;
	  cout<<"Simulation::statsFileUpdate() - Exiting..."<<endl;
	  parallel.abortRequest();
	}

      statsFile_.precision(7);
      statsFile_.width(statsFile_.precision()+7);
      statsFile_.setf(fstream::scientific | fstream::showpos);
      
      parallel.barrier();

      //Write time
      this->statsFileLine();
      statsFile_<<endl;
      statsFile_.close();
    }
  else
    {
      parallel.barrier();
      this->statsFileLine();
    }
}


//HIBERNATE FUNCTIONS================================

void Simulation::hibernateInitialize(bool wake, string preString, string postString)
{
  hibernateWaking_ = wake;
  hibernatePreString_ = preString;
  hibernatePostString_ = postString;
}

void Simulation::hibernate()
{
  if(parallel.isRoot())
    {
      string filename=hibernatePreString_+"hibernateSimulation"+hibernatePostString_;
      hibernateFile_.open( filename.c_str(), ios::out | ios::trunc );
      
      if(!hibernateFile_.good())
	{
	  COUT<<"Simulation::hibernate() - Cannot write to file: "<<filename<<endl;
	  COUT<<"Simulation::hibernate() - Exiting..."<<endl;
	  exit(EXIT_FAILURE);
	}
      
      hibernateFile_.write( (char*) &t, sizeof(Real) );
      hibernateFile_.write( (char*) &timeStep, sizeof(int) );
      hibernateFile_.write( (char*) &emConservationStatus_, sizeof(int) );
      hibernateFile_.write( (char*) &timeStep2, sizeof(int) );

      this->hibernateSave();
      
      hibernateFile_.close();
    }
  else
    {
      this->hibernateSave();
    }
}

void Simulation::hibernateWake()
{
  if(parallel.isRoot())
    {
      string filename=hibernatePreString_+"hibernateSimulation"+hibernatePostString_;
      hibernateFile_.open( filename.c_str(), ios::in );
      
      if(!hibernateFile_.good())
	{
	  COUT<<"Simulation::hibernateWake() - Cannot read from file: "<<filename<<endl;
	  COUT<<"Simulation::hibernateWake() - Exiting..."<<endl;
	  exit(EXIT_FAILURE);
	}
      
      hibernateFile_.read( (char*) &t, sizeof(Real) );
      hibernateFile_.read( (char*) &timeStep, sizeof(int) );
      hibernateFile_.read( (char*) &emConservationStatus_, sizeof(int) );
      hibernateFile_.read( (char*) &timeStep2, sizeof(int) );

      parallel.broadcast(t, parallel.root());
      parallel.broadcast(timeStep, parallel.root());
      parallel.broadcast(emConservationStatus_, parallel.root());
      parallel.broadcast(timeStep2, parallel.root());

      this->hibernateLoad();

      hibernateFile_.close();
    }
  else
    {
      parallel.broadcast(t, parallel.root());
      parallel.broadcast(timeStep, parallel.root());
      parallel.broadcast(emConservationStatus_, parallel.root());
      parallel.broadcast(timeStep2, parallel.root());

      this->hibernateLoad();
    }
}


#endif
