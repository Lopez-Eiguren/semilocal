#ifndef HOURMINSEC_HPP
#define HOURMINSEC_HPP

#include <string>

double seconds(clock_t end, clock_t start)
{
  return double(end-start)/CLOCKS_PER_SEC;
}

string hourMinSec(double time)
{
  int hour=int(floor(time/3600.0)); time-=3600*hour;
  int min=int(floor(time/60.0)); time-=60*min;
  int sec=int(floor(time)); time-=sec;
  
  string output;
  char c;
  
  c='0'+(hour-hour%100)/100; hour=hour%100; output=c;
  c='0'+(hour-hour%10)/10; hour=hour%10; output+=c;
  c='0'+hour; output+=c;
  
  output+=':';

  c='0'+(min-min%10)/10; min=min%10;   output+=c;
  c='0'+min; output+=c;

  output+=':';

  c='0'+(sec-sec%10)/10; sec=sec%10;   output+=c;
  c='0'+sec; output+=c;

  output+='.';
  c='0'+time*10; output+=c;

  return output;
}

string hourMinSec(clock_t end, clock_t start)
{
  return hourMinSec(seconds(end,start));
}

#endif
